# Introduction

## Core idea

At its core, a certificate is a public key bundled with some metadata, and a
signature on the bundle.

Everything else flows from this.


## About this document

This document exists to provide practical instruction, founded in theory, on
how to work with certificates.

It is intended to give you bootstrap knowledge.
That is, it does not assume that you have prior experience with cryptography or
working with certificates.
As such, priority is given to clearly presenting foundational information over
depth of information.
If we find a way to present a greater depth and more nuanced information without
muddling the core topics, it may be included in the future.
