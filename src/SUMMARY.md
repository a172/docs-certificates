[Introduction](README.md)

# Theory

- [Cryptography]()
  - [Cryptographic Goals](theory/crypto/goals.md)
  - [Cryptographic Functions](theory/crypto/functions.md)
- [Certificates](theory/certs/README.md)
  - [PKI and trust chains](theory/certs/pki.md)
  - [CA/Browser forum](theory/certs/cab.md)
  - [Getting certificates signed](theory/certs/csr.md)

# Practice

- [File formats](practice/formats.md)
- [OpenSSL examples](practice/openssl/README.md)
  - [crt](practice/openssl/crt.md)
  - [key](practice/openssl/key.md)
  - [req](practice/openssl/req.md)
  - [conf](practice/openssl/conf.md)
