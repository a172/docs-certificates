# X509 Request Configuration

> **NOTE**: This page is written with Virginia Tech's NEO certificate repository
> in mind.
> It won't make much sense outside of this context.

## Ehhh...

These files are pretty optional, especially as many of our certificates will
have a CSR generated on the device.
However, it can be convenient to have CSR config file to create a CSR that
includes the subject alternate names (SANs) and settings that will be used when
the request is signed.
SANs included in the request will automatically be included in the request
process.
Other SANs can be specified in the web form during the request process.

## Usage

Use `template.conf` as a template.
This references a file which includes fields that are the same for all requests.

In a specific certificate, you should change:

* `commonName`:
  This is the FQDN Common Name of the server.
* `DNS.#`:
  Subject Alternate names.
  This should include the Common Name and any other names that may be useful.
  In particular, we often want to include the `.ipv4.vt.edu` and `.ipv6.vt.edu`
  domains.
  Add as many of these as is needed, just make sure they all have a different
  number.

## Email field:

Currently, the email address part of the subject is not included in the actual
certificate, so this section is mostly academic.
By convention / for reference, we have this field on the request match the
email that is entered in the request process for notifications about the
certificate expiring.

Additionally, although Middleware has taken over responsibility of LDAP (and
the associated certificates), the certificates are still in this repository
(for now).

* `.include`:
  This references the file that includes the standard fields.
  Note that the path is _relative to where the openssl command is run from_.
  For consistency, we assume this to be the repository root
  (e.g., `conf/neo.conf`).
  * `vt.conf`:
    Includes all the fields that remain consistent for any VT certificate.
  * `neo.conf`:
    Includes `vt.conf` and the email for NEO.
    This is probably the file you want to include.
  * `middleware.conf`:
    Includes `vt.conf` and the email for Middleware.

Once Middleware moves the LDAP certificates to their own repository,
`middleware.conf` will be removed, and `vt.conf` will probably merge into
`neo.conf`.
