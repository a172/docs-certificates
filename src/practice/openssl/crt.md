# Certificates

## File Format

Follow these guidelines when adding certificates:

* **Use the CN as the file name, and crt as the extension.**
  E.g., if your CN is 'example.nis.vt.edu', then the file name should be
  'example.nis.vt.edu.crt'.

* **Use PEM format.**
  Using a format that can be edited in a text editor
  simplifies the workflow.

* **Include comments.**
  This is convenient when building a chain.
  In order, the following comments should be included, before the certificate:

    1. Subject
    2. Issuer
    3. Dates

* **Terminate the last line with an `EOL`.**
  The certificate downloaded from certs.it.vt.edu omits this, which breaks
  `cat`-ing together a chain.

tldr:

```bash
openssl x509 -in CN.crt -subject -issuer -dates | sponge CN.crt
```

## Creating a chained file

### X509

Just `cat` the files together. The order should be:

1. Leaf certificate
2. Private Key
3. Intermediate certs

See the README in the key directory for notes on re-encrypting the private key!
Attaching the PGP encrypted key will _not_ work.

```bash
cat \
  CN.crt \
  CN.rsa \
  InCommonECCServerCA2.crt \
  > CN_chain.pem
```

### PKCS12

PKCS12, or p12, is commonly used by Microsoft servers and a few other things.
It packages together the cert, key, and the chain into a single structure.

```bash
openssl pkcs12 \
  -export \
  -in CN.crt \
  -certfile InCommonECCServerCA2.crt \
  -inkey <(gpg -qd CN.rsa.asc) \
  -passout file:<(gpg -qd pwfile.pgp) \
  -out CN.p12
```

If you have more than one intermediate certificate, you can create the necessary
bundle on the fly with:
```
-certfile <(cat intermediate1.crt intermediate2.crt ...)
```

## Tips

The server should always send the full chain (excluding the root), not just the
leaf certificate.
You can verify this with the `openssl s_client` command.

* Use `-connect <server>:<port>` to specify what server to connect to.
* Passing something to stdin closes the connection which avoids staying in
  interactive mode.
* Use `-verify_hostname <hostname>` to validate the certificate is valid for the
  given name.
* Use `-verify_return_error` to validate the chain (this will kill the
  connection on error, which may leave out useful information).
* Use `-CAfile <root cert>` to specify what root CA should be used to verify
  the certificate chain.
* Use `-attime <ts>` to test the validity of the certificate at
  _&lt;ts&gt;_ seconds since epoch.
  * This is particularly useful for testing a certificate renewal.
  * The combination of a few shell tricks is likely useful here:
    * [`DATE(1)`](https://www.man7.org/linux/man-pages/man1/date.1.html) to get
      the timestamp at a particular time
    * Bash arithmetic with the `$(( ... ))` notation to get an offset from
      current time
* When an `EOF` is received, the connection is automatically closed.
  Passing an empty string to stdin is sufficient.

Putting it all together:

```bash
openssl s_client \
    -connect example.nis.vt.edu:443 \
    -verify_hostname example.nis.vt.edu \
    -verify_return_error \
    -CAfile crt/chain/USERTrust_ECC_Certification_Authority.crt \
    -attime "$(( $(date '+%s') + 45 * 24 * 60 * 60 ))" \
    <<< ""
```
