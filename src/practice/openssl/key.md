# Private Keys

OpenSSL provides several ways to accomplish many of these tasks.
Where possible, the algorithm agnostic subcommands have been chosen for the sake
of consistency and ease of understanding.

## File names

Our convention is to use the FQDN as the file name, and the key type as the
extension, with an additional extension indicating the encryption method.
For example, `netmri.nis.vt.edu.rsa.asc`.


## Generating keys

In all cases, this will output the cleartext private key to stdout.
This should be encrypted before writing to disk (see
[below](#securing-keys-at-rest)).

### RSA

RSA keys have the widest support, but are also the slowest.
```bash
openssl genpkey \
  -algorithm RSA \
  -pkeyopt rsa_keygen_bits:2048 \
  -quiet
```

Alternatively:
```bash
openssl genrsa 2048
```

Use the `rsa` extension in filenames.

### P-256

ECC keys are much smaller, thus faster, while maintaining security.
Browser support is ubiquitous, but you may hit compatibility issues elsewhere.
```bash
openssl genpkey \
  -algorithm EC \
  -pkeyopt ec_paramgen_curve:P-256 \
  -pkeyopt ec_param_enc:named_curve
```

Use the `p256` extension in filenames.

### Ed25519

**WARNING:** Ed25519 support is _very_ experimental.
```bash
openssl genpkey -algorithm ed25519
```

Use the `ed25519` extension in filenames.

## Securing keys at rest

Private keys are sensitive material!
**Cleartext private keys must never be written to disk.**
The solution is to encrypt the key before writing them to the disk.

In these examples, the commands start with `... | ` to indicate a cleartext key
is expected on stdin.

In these examples, output is to stdout.

### Encrypting with PGP
This method wraps a cleartext private key with PGP.
This is a convenient way to securely share private keys with colleagues.
Notably, all private keys in the NEO certificate repository MUST be encrypted
with PGP.

```bash
... | gpg -ear <your-key-id> -r <colleague1> ...
```

Our convention is to ascii armor the output and use the `.asc` extension in
filenames.

### Encrypting with a passphrase
This method encrypts a private key with a passphrase in a way that is well
understood by most things that speak x509.

Because of the complications of using a shared passphrase, this is most
appropriate where the lifetime of the passphrase is a single task (e.g.,
importing a private key to a network appliance).

First, generate a temporary passphrase to use.
We'll want to use it later, but we also don't want to write this cleartext
passphrase to disk.

```bash
pwgen -s 22 | gpg -eao pwfile.asc
```

Then, use this passphrase to encrypt the file:
```bash
... | openssl pkey \
  -passout file:<(gpg -qd < pwfile.asc) \
  -aes256
```

See [openssl-passphrase-options(1)][openssl-passphrase-options1] for valid
options for the `-passout` argument.

[openssl-passphrase-options1]: https://www.openssl.org/docs/manmaster/man1/openssl-passphrase-options.html
