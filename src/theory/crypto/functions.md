# Cryptographic Functions

## Encryption

Encryption is a reversible function that makes the message unreadable without
the decryption key.

_Encryption is related to the goal of privacy._

### Symmetric encryption

Symmetric encryption is a method of encrypting where the encryption key and the
decryption key are the same (or the decryption key is trivially derived from the
encryption key).

An academic example would be the Caesar cipher.
This is where you take the letters of the message and shift them by some number
of letters.
Suppose we had the message "HELLOWORLD" and a key of 5.
The encrypted message would be "MJQQTBTWQI" and the decryption key would be -5.

In practice, **use AES**.
AES-128 and AES-256 are common, and both are [plenty secure][3b1b].


### Asymmetric encryption

Asymmetric encryption is where the decryption key cannot be derived from the
encryption key.

This allows someone to freely distribute the encryption key to let anyone send
them an encrypted message.
Here the encryption key is often called the public key, and the decryption key
is called the private key.

In practice, **use RSA** (at least 2048 bits). X25519 is also good, but may have
limited support.

## Hashing

A cryptographic hash takes a message as input and outputs a bit string of a
fixed size, that is, a hash.

_Hashing is related to the goal of integrity._

A good hash function has the following properties:

* **Deterministic** - the same message will always result in the same hash.
* **Fast** - Computing the hash is resource light.
* **Preimage resistance** - It is infeasible to go from a hash to a message.
* **Second preimage resistance** - Given a message, it is infeasible to find a
  distinct message with the same hash.
* **Collision resistance** - It is infeasible to generate two messages with the
  same hash.

In short, a small change in the message produces a wildly different hash.

In practice, **use SHA-2**.
This family of hashes includes:

* SHA-256
* SHA-512

The following hashes **no longer have collision resistance**:

* MD4
* MD5
* SHA-1

## Signing

A signature verifies the authenticity of a message.

_Signing is related to the goal of authenticity, and depends on integrity._

By its nature, a signature is asymmetric.
The author of the message uses a private key to sign the message.
The message can then be verified with the public key.
Typically, it is the hash of the message that is signed, not the message itself.

In practice, **use RSA** (at least 2048 bits) or **P-256**. Ed25519 is also
good, but may have limited support.
_Avoid_ DSA.

## Key exchange

A key exchange is a method that allows two users on an public channel to
negotiate a shared secret, which can then be used as a symmetric encryption key.

_Key exchange is related to the goal of channel elevation._

[3b1b]: https://www.youtube.com/watch?v=S9JGmA5_unY
